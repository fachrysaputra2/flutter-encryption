# encryption_test

test encryption from node js to flutter

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## penjelasan enkripsi
- plain string untuk iv dan key :
    1. plain string iv dan key di hash sha256 utf8
    2. hasil hash yang digunakan untuk iv adalah 16 digit pertama , dan untuk key 32 digit pertama
- enkripsi dengan AES , method cbc
- hasil enkripsi yg digunakan = base64 (nantinya mungkin dari be hasil nya perlu dilakukan encode
 ke base 64 dulu agar hasilnya sama)
- dekripsi dilakukan dengan men-decode hasil enkripsi , kemudian baru didekripsi dengan metode
yang sama


