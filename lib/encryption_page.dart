import 'package:encrypt/encrypt.dart' as encrypt_pack;
import 'package:crypto/crypto.dart' as crypto_pack;
import 'dart:convert' as convert_pack;
import 'package:flutter/material.dart';

class EncyptionPage extends StatefulWidget {
  const EncyptionPage({Key? key}) : super(key: key);

  @override
  _EncyptionPageState createState() => _EncyptionPageState();
}

class _EncyptionPageState extends State<EncyptionPage> {
  TextEditingController control = TextEditingController();
  TextEditingController resultControl = TextEditingController();
  String strPwd = "QYzSX1uGp5/VTA2v888Ji6MStIzfkTlk";
  String strIv = '9a600fdfa7605644';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              Flexible(
                child: Container(
                  padding: const EdgeInsets.all(8),
                  margin: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: const BorderRadius.all(Radius.circular(16))
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Input"),
                      TextFormField(
                        controller: control,

                      ),
                    ],
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  padding: const EdgeInsets.all(8),
                  margin: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: const BorderRadius.all(Radius.circular(16))
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Output"),
                      TextFormField(
                        controller: resultControl,

                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: (){
                try {
                  // /// 16 first digit of 64
                  var iv = crypto_pack.sha256.convert(
                      convert_pack.utf8.encode(strIv))
                      .toString().substring(0, 16);
                  // /// 32 first digit of 64
                  var key = crypto_pack.sha256.convert(convert_pack.utf8.encode
                    (strPwd))
                      .toString().substring(0, 32);
                  encrypt_pack.IV ivObj = encrypt_pack.IV.fromUtf8(iv);
                  encrypt_pack.Key keyObj = encrypt_pack.Key.fromUtf8(key);
                  final encrypter = encrypt_pack.Encrypter(
                      encrypt_pack.AES(keyObj, mode:
                      encrypt_pack.AESMode.cbc));
                  final encrypt = encrypter.encrypt(control.text,iv: ivObj);
                  setState(() {
                    resultControl.text = encrypt.base64;
                  });
                }catch(e){
                  setState(() {
                    resultControl.text = e.toString();
                  });
                }
              }, child: const Text
                ("Encrypt")),
              const SizedBox(width: 20,),
              ElevatedButton(onPressed: (){
                try {
                  // /// 16 first digit of 64
                  var iv = crypto_pack.sha256.convert(
                      convert_pack.utf8.encode(strIv))
                      .toString().substring(0, 16);
                  // /// 32 first digit of 64
                  var key = crypto_pack.sha256.convert(convert_pack.utf8.encode
                    (strPwd))
                      .toString().substring(0, 32);
                  encrypt_pack.IV ivObj = encrypt_pack.IV.fromUtf8(iv);
                  encrypt_pack.Key keyObj = encrypt_pack.Key.fromUtf8(key);
                  final encrypter = encrypt_pack.Encrypter(
                      encrypt_pack.AES(keyObj, mode:
                      encrypt_pack.AESMode.cbc));
                  // final encrypt = encrypter.encrypt(control.text,iv: ivObj);
                  final decrypt = encrypter.decrypt(encrypt_pack.Encrypted
                      .fromBase64(control.text),iv: ivObj);
                  setState(() {
                    resultControl.text = decrypt;
                  });
                }catch(e){
                  setState(() {
                    resultControl.text = e.toString();
                  });
                }
              }, child: const Text
                ("Decrypt")),
            ],
          ),
        ],
      ),
    );
  }
}

